CREATE DATABASE INTRO_USERS_9989636
GO

Drop Database INTRO_USERS_9989636

USE INTRO_USERS_9989636
GO

CREATE TABLE Type (
		name varchar(20)  PRIMARY KEY,
		cost varchar(10) NOT NULL,
		hours int NOT NULL
		
	)

CREATE TABLE Client (
	username varchar(20) NOT NULL PRIMARY KEY,
	firstname varchar(20) NOT NULL,
	lastname varchar(20) NOT NULL,
	email varchar(50) NOT NULL,
	Password varchar(20) NOT NULL,
	ctype varchar(20),

)

CREATE TABLE Document (
		docID varchar(20) NOT NULL PRIMARY KEY,
		link varchar(100) NOT NULL,
		date varchar(9) NOT NULL,
		type varchar(10) NOT NULL
	)



CREATE TABLE Appointment (
		appID int NOT NULL IDENTITY (1,1) PRIMARY KEY,
		notes varchar(150),
		
	)



CREATE TABLE Admin (
		username varchar(20) NOT NULL PRIMARY KEY,
		firstname varchar(15) NOT NULL,
		lastname varchar(15) NOT NULL,
		email varchar(40) NOT NULL,
		password varchar(20) NOT NULL
		
	)

		
	


CREATE TABLE car (
		License varchar(20) NOT NULL PRIMARY KEY,
		make varchar(15) NOT NULL,
		
		
	)


CREATE TABLE Instructors (
	Username varchar(50) NOT NULL PRIMARY KEY,
	Password varchar(50) NOT NULL,
	FirstName varchar(50) NOT NULL,
	LastName varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	phone varchar(50) NOT NULL
	)

CREATE TABLE received (
		client_username varchar(20) NOT NULL,
		docID varchar(20) NOT NULL
		primary key(client_username,docID)
		foreign key(client_username) references Client,
		foreign key(docID) references Document

		)

CREATE TABLE books (
		client_username varchar(20) NOT NULL,
		appointmentID int NOT NULL,
		primary key(client_username,appointmentID),
		foreign key(client_username) references Client,
		foreign key(appointmentID) references Appointment
		)




CREATE TABLE times(
             date int,
			 time varchar(10),
			 instructor varchar(30),
			 avil int,
			 corse varchar (12),
			 car varchar(10),
			 client varchar(20)
			 )


CREATE TABLE Timeslot (
    Id int IDENTITY(1,1) NOT NULL,
    Time varchar(50) NOT NULL,
    Date varchar(100) NOT NULL,
    Instructorname varchar(50) NOT NULL, 
    Client varchar(50) NOT NULL,
    PRIMARY KEY (Id, Time, Date),
    )		






DROP TABLE Admin
DROP TABLE Client
DROP TABLE books

DROP TABLE car
DROP TABLE Timeslot
DROP TABLE Instructors
DROP TABLE received
DROP TABLE Type
DROP TABLE Appointment
DROP TABLE Document
DROP TABLE times



INSERT INTO car VALUES ('abc123', 'Nissan')
INSERT INTO car VALUES ('hdh222', 'Toyota')
INSERT INTO car VALUES ('hel900', 'Mazda')
INSERT INTO car VALUES ('hyt333', 'Nissan')
INSERT INTO car VALUES ('xvx122', 'Volvo')



INSERT INTO Instructors Values ('bob', 'bob1','bob','burger','bob@car.com','0278865841')
INSERT INTO Instructors Values ('frank', 'frank1','frank','high','frank@car.com','0271146985')
INSERT INTO Instructors Values ('josh', 'josh1','josh','smith','josh@car.com','0278555471')
INSERT INTO Instructors Values ('ted', 'ted1','ted','lost','tedback@car.com','0278888881')

INSERT INTO Admin Values ('poppy1', 'poppy','smith','poppysm@car.com','poppysm33')
INSERT INTO Admin Values ('mary21', 'mary','jones','maryj@car.com','maryj37')
INSERT INTO Admin Values ('adam99', 'adam','bigtooth','adambig@car.com','adam99')

INSERT INTO Type Values ('beginner', '$75',3)
INSERT INTO Type Values ('Advanced', '$40',2)



INSERT INTO Appointment Values ( 'needs work on parking')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ('work on checking')
INSERT INTO Appointment Values ( 'needs work on parking')
INSERT INTO Appointment Values ( 'needs work on parking')
INSERT INTO Appointment Values ( 'work on 3 point turn')
INSERT INTO Appointment Values ( 'work on checking')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'work on checking')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'work on checking')
INSERT INTO Appointment Values ( 'needs work on parking')
INSERT INTO Appointment Values ( 'needs work on parking')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'work on 3 point turn')
INSERT INTO Appointment Values ( '')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'work on 3 point turn')
INSERT INTO Appointment Values ( '')
INSERT INTO Appointment Values ('final lesson')
INSERT INTO Appointment Values ('work on 3 point turn')
INSERT INTO Appointment Values ( 'yessir')
INSERT INTO Appointment Values ( 'final lesson')
INSERT INTO Appointment Values ( 'work on 3 point turn')
INSERT INTO Appointment Values ('')
INSERT INTO Appointment Values ( '')
INSERT INTO Appointment Values ( 'work on 3 point turn')
INSERT INTO Appointment Values ( '')
INSERT INTO Appointment Values ( 'work on 3 point turn')

INSERT INTO timeslot (appID,time,date) Values (1, '7:30am','1/5/2017')
INSERT INTO timeslot (appID,time,date) Values (2, '9:30am','2/5/2017')
INSERT INTO timeslot (appID,time,date) Values (3, '2:00pm','3/5/2017')
INSERT INTO timeslot (appID,time,date) Values (4, '8:40am','4/5/2017')
INSERT INTO timeslot (appID,time,date) Values (5, '7:30am','5/5/2017')
INSERT INTO timeslot (appID,time,date) Values (6, '10:00am','6/5/2017')
INSERT INTO timeslot (appID,time,date) Values (7, '8:30am','7/5/2017')
INSERT INTO timeslot (appID,time,date) Values (8, '1:30pm','8/5/2017')
INSERT INTO timeslot (appID,time,date) Values (9, '10:30am','9/5/2017')
INSERT INTO timeslot (appID,time,date) Values (10, '11:30am','10/5/2017')
INSERT INTO timeslot (appID,time,date) Values (11, '8:30am','11/5/2017')
INSERT INTO timeslot (appID,time,date) Values (12, '3:00pm','12/5/2017')
INSERT INTO timeslot (appID,time,date) Values (13, '12:00pm','13/5/2017')
INSERT INTO timeslot (appID,time,date) Values (14, '2:00pm','14/5/2017')
INSERT INTO timeslot (appID,time,date) Values (15, '9:00am','15/5/2017')
INSERT INTO timeslot (appID,time,date) Values (16, '7:30am','16/5/2017')
INSERT INTO timeslot (appID,time,date) Values (17, '9:30am','17/5/2017')
INSERT INTO timeslot (appID,time,date) Values (18, '2:00pm','18/5/2017')
INSERT INTO timeslot (appID,time,date) Values (19, '8:40am','19/5/2017')
INSERT INTO timeslot (appID,time,date) Values (20, '7:30am','20/5/2017')
INSERT INTO timeslot (appID,time,date) Values (21, '10:00am','21/5/2017')
INSERT INTO timeslot (appID,time,date) Values (22, '8:30am','22/5/2017')
INSERT INTO timeslot (appID,time,date) Values (23, '1:30pm','23/5/2017')
INSERT INTO timeslot (appID,time,date) Values (24, '10:30am','24/5/2017')
INSERT INTO timeslot (appID,time,date) Values (25, '11:30am','25/5/2017')
INSERT INTO timeslot (appID,time,date) Values (26, '8:30am','26/5/2017')
INSERT INTO timeslot (appID,time,date) Values (27, '3:00pm','27/5/2017')
INSERT INTO timeslot (appID,time,date) Values (28, '12:00pm','28/5/2017')
INSERT INTO timeslot (appID,time,date) Values (29, '2:00pm','29/5/2017')
INSERT INTO timeslot (appID,time,date) Values (30, '9:00am','30/5/2017')




--select statement
Select * FROM car

Select * FROM Type
select * FROM timeslot ORDER BY timeID ASC
select * FROM appointment ORDER BY appID ASC
select * FROM Client
Select * FROM Instructors
Select * FROM Admin
Select * FROM Timeslot


