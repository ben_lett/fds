﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class admintime : Form
    {


        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS_9989636;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public string user = "";

        public void stuff()
        {
            con.Open();
            cmd.CommandText = "select Date, Time, InstructorName, Client from Timeslot";
            cmd.Connection = con;
            read = cmd.ExecuteReader();

            while (read.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read["Date"].ToString());
                item.SubItems.Add(read["Time"].ToString());
                item.SubItems.Add(read["InstructorName"].ToString());
                item.SubItems.Add(read["Client"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public admintime(string Currentuser)
        {
            InitializeComponent();


            label9.Text = Currentuser;
            user = label9.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void admintime_Load(object sender, EventArgs e)
        {
            stuff();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            //Create a Register Page object to change to
            LoginPage register = new LoginPage();
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();

        }
    }
}