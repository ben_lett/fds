﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTRO_USERS
{
    public partial class clienttime : Form
    {

        SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=INTRO_USERS_9989636;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader read;

        public string user = "";

        public void stuff()
        {
            con.Open();
            cmd.CommandText = "select Date, Time, InstructorName from Timeslot";
            cmd.Connection = con;
            read = cmd.ExecuteReader();

            while (read.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(read["Date"].ToString());
                item.SubItems.Add(read["Time"].ToString());
                item.SubItems.Add(read["InstructorName"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        public clienttime(string Currentuser)
        {
            InitializeComponent();

            user = Currentuser;
            label9.Text = user;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void clienttime_Load(object sender, EventArgs e)
        {
            stuff();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            
            //Create a Register Page object to change to
            LoginPage register = new LoginPage();
            //show the register page
            register.ShowDialog();
            //close the login page we are currently on
            this.Close();
            
        }
    }
}
